package org.task;

import java.util.Arrays;

public class copyarrayIterating {
	public static void main(String[] args) {
		int[] array = {25,14,56,15,36,56,77};
		int[] copy_array = new int[array.length];
		
		System.out.println("before copy: "+ Arrays.toString(copy_array));
		
		for (int i = 0; i < array.length; i++) {
			copy_array[i] = array[i];
			
		}
		
		System.out.println("after copy: "+Arrays.toString(copy_array));
	}

}
