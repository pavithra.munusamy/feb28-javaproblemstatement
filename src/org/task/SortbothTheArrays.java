package org.task;

import java.util.Arrays;

public class SortbothTheArrays {
	
	public static void main(String[] args) {
		
		int[] numeric_array1= { 17,89,2,35,99,14,56,20,3};
		
		String[] string_array2= { "Orange", "Apple", "Mango"};
		
		System.out.println("Given numeric array :" +Arrays.toString(numeric_array1));
		Arrays.sort(numeric_array1);
		System.out.println("sorted numeric array :" +Arrays.toString(numeric_array1));
		
		System.out.println("Given String array :" + Arrays.toString(string_array2));
		Arrays.sort(string_array2);
		
		System.out.println("Sorted string array :" +Arrays.toString(string_array2));
	}

}
