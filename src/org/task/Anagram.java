package org.task;

import java.util.Arrays;

public class Anagram {
	
	public static void main(String[] args) {
		String s1 = "Lake";
		String s2 = "kale";
		
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		
		if(s1.length() == s2.length()) {
			
			char[] array1 = s1.toCharArray();
			char[] array2 = s1.toCharArray();
			
			Arrays.sort(array1);
			Arrays.sort(array2);
			
			boolean result = Arrays.equals(array1, array2);
			
			if (result) {
				System.out.println(s1 + " and " + s2 + "  are anagram ");
			}
			else {
				System.out.println(s1 + " and " + s2 + " not anagram ");
			}
			
		}
		else {
			System.out.println(s1 + " and " + s2 + " not anagram ");
		}
				
	}		

}