package org.task;

import java.util.Scanner;

public class RemoveDuplicateString {

	public static void main(String[] args) {
		
		String given = "pavithra";
		
		String answer = "";
		
		for (int i = 0; i < given.length(); i++) {
			
				if (!answer.contains(given.charAt(i) + "")) {
					System.out.println(given.charAt(i));
					answer = answer + given.charAt(i);
				}
		}
				
				System.out.println(answer);
		}
}
